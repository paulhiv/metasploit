# Utilisation du script

- ouvrez le .backupconf et changez les varriables avec le votres
- lancez la commande `` ./backupdb ``
- editez votre crontab avec `` crontab -e ``
    ```
        0 0 * * 0,3,6 $HOME/script
        0 0 * * 1,4 $HOME/cleanbackup 
    ```
- utilisez ``./backupSpecificDb`` pour créer une archive d'une database spécifique
- utilisez ``./restoreDb votreArchive`` pour restaurer une base de donné archivée

# variables

- user= votre utulisetaur
- password= votre mot de passe
- dbname= le nom de la base de donné a backup
- logPath= ou vous voulez mettre vos logs du script
- regex= ne pas toucher
- backupName= ne pas toucher
